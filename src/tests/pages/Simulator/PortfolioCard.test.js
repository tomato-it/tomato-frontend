import { PortfolioCard } from '../../../pages/Simulator/PortfolioCard/PortfolioCard';
import React from 'react';
import { shallow } from 'enzyme';

describe('Test <PortfolioCard />', () => {
    const PortfolioCardProps = {
        onSelectPortfolio: () => null,
        title: 'Aggressive',
        selected: false,
    };

    let wrapper = shallow(<PortfolioCard {...PortfolioCardProps} />);
    beforeEach(() => {
        wrapper = shallow(<PortfolioCard {...PortfolioCardProps} />);
    });

    test('should display <PortfolioCard /> correctly ', () => {
        expect(wrapper).toMatchSnapshot();
    });

    test('should show p.title sent by props', () => {
        const pText = wrapper.find('p').text();

        expect(pText).toBe(PortfolioCardProps.title);
    });

    test('should PortfolioCard with class selected', () => {
        const selected = true;
        const wrapper = shallow(<PortfolioCard {...PortfolioCardProps} selected={selected} />);

        const isSelected = wrapper.find('.card-item').hasClass('selected');
        expect(isSelected).toEqual(true);
    });
});
