import { renderHook, act } from '@testing-library/react-hooks';
import { useSimulator } from './../../hooks/useSimulator';
import { ChartPortfolioFilters } from './../../pages/Simulator/Simulator.constant';

describe('Test customHook useSimulator', () => {
    const defaultState = {
        loading: true,
        portfolios: [],
        errorMessage: '',
        selectedPortfolio: '',
        onSelectPortfolio: () => null,
        lineChart: {
            balance: { totalBalance: 0, variation: 0, amountVariation: 0 },
            data: undefined,
            filter: {
                value: ChartPortfolioFilters[ChartPortfolioFilters.length - 1],
                onChange: () => null,
            },
            initialCapital: {
                value: 10000,
                onChange: () => null,
            },
        },
    };

    test('should return default value state ', async () => {
        const { result } = renderHook(() => useSimulator());

        const {
            loading,
            portfolios,
            errorMessage,
            lineChart: { balance, filter, initialCapital },
        } = result.current;

        expect(loading).toBe(defaultState.loading);
        expect(portfolios).toEqual(defaultState.portfolios);
        expect(errorMessage).toBe(defaultState.errorMessage);
        expect(balance).toMatchObject(defaultState.lineChart.balance);
        expect(filter.value).toBe(defaultState.lineChart.filter.value);
        expect(initialCapital.value).toBe(defaultState.lineChart.initialCapital.value);
    });

    test('should load portfolios ', async () => {
        const { result, waitForNextUpdate } = renderHook(() => useSimulator());

        await waitForNextUpdate({ timeout: 6000 });

        const { portfolios } = result.current;

        expect(portfolios.length).toBeGreaterThanOrEqual(1);
    });

    test('should change selectedPorfolio', async () => {
        const { result, waitForNextUpdate } = renderHook(() => useSimulator());

        await waitForNextUpdate({ timeout: 6000 });

        const { portfolios, onSelectPortfolio } = result.current;

        const portfolioToSelect = portfolios[0].slug;
        act(() => {
            onSelectPortfolio(portfolioToSelect);
        });
        const { selectedPortfolio } = result.current;

        expect(selectedPortfolio).toEqual(portfolioToSelect);
    });

    test('should change selectedPorfolio', async () => {
        const { result, waitForNextUpdate } = renderHook(() => useSimulator());

        await waitForNextUpdate({ timeout: 6000 });

        const { portfolios, onSelectPortfolio } = result.current;

        const portfolioToSelect = portfolios[0].slug;
        act(() => {
            onSelectPortfolio(portfolioToSelect);
        });
        const { selectedPortfolio } = result.current;

        expect(selectedPortfolio).toEqual(portfolioToSelect);
    });

    test('should change initialCapital to 30000', async () => {
        const newInitialCapital = 30000;
        const { result, waitForNextUpdate } = renderHook(() => useSimulator());

        await waitForNextUpdate({ timeout: 6000 });

        const {
            lineChart: {
                initialCapital: { onChange },
            },
        } = result.current;

        act(() => {
            onChange(newInitialCapital);
        });

        const { value } = result.current.lineChart.initialCapital;

        expect(value).toEqual(newInitialCapital);
    });
});
