import { roundNumber } from '../../../utils/number/roundNumber';

describe('Test roundNumber.ts', () => {
    test('send num 3.234 should return 3.23', () => {
        const num = 3.234;
        const result = 3.23;

        expect(roundNumber(num)).toBe(result);
    });
    test('send num 3.235 should return 3.24', () => {
        const num = 3.235;
        const result = 3.24;

        expect(roundNumber(num)).toBe(result);
    });
});
