import React, { LazyExoticComponent, lazy } from 'react';

type JSXComponent = () => JSX.Element;
type routeType = 'private' | 'public';

interface Route {
    to: string;
    path: string;
    Component: LazyExoticComponent<JSXComponent> | JSXComponent;
    name: string;
    type: routeType;
}

const LazyLogin = lazy(() => import('../pages/Login/Login'));
const LazyQuestions = lazy(() => import('../pages/Questions/Questions'));
const LazySimulator = lazy(() => import('../pages/Simulator/Simulator'));

export const routes: Route[] = [
    {
        path: '/login',
        to: '/login',
        Component: LazyLogin,
        name: 'Login',
        type: 'public',
    },
    {
        path: '/simulator',
        to: '/simulator',
        Component: LazySimulator,
        name: 'Simulator',
        type: 'private',
    },
    {
        path: '/questions',
        to: '/questions',
        Component: LazyQuestions,
        name: 'Questions',
        type: 'private',
    },
];
