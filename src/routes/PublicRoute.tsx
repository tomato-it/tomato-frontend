import React from 'react';
import { useNavigate, useLocation, Navigate } from 'react-router-dom';
import { const_user } from '../utils/constants';

interface PublicRouteProps {
    children: JSX.Element;
}

export const PublicRoute = ({ children }: PublicRouteProps) => {
    const location = useLocation();
    const isLogged = localStorage.getItem(const_user.user_email);
    console.log('public ', location, 'isLogged ', isLogged);

    return isLogged ? <Navigate to={'/simulator'} replace={true} /> : children;
};
