import React, { Suspense } from 'react';
import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';
import { PrivateRoute } from './PrivateRoute';
import { PublicRoute } from './PublicRoute';
import { routes } from './routes';
import { WrapperRoute } from './WrapperRoute/WrapperRoute';

export const Navigation = () => {
    return (
        <Suspense fallback={<h4>Loading...</h4>}>
            <BrowserRouter>
                <WrapperRoute>
                    <Routes>
                        {/* <Route path="/" element={<Navigate to={routes[0].path} replace />}></Route> */}
                        {routes.map(({ path, Component, type }) => (
                            <Route
                                key={path}
                                path={path}
                                element={
                                    type === 'private' ? (
                                        <PrivateRoute>
                                            <Component />
                                        </PrivateRoute>
                                    ) : (
                                        <PublicRoute>
                                            <Component />
                                        </PublicRoute>
                                    )
                                }
                            />
                        ))}
                        <Route
                            path="*"
                            element={
                                <main style={{ padding: '1rem' }}>
                                    <p>There's nothing here!</p>
                                    <Navigate to={routes.find((r) => r.type === 'public').path} replace />
                                </main>
                            }
                        />
                    </Routes>
                </WrapperRoute>
            </BrowserRouter>
        </Suspense>
    );
};
