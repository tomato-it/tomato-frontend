import React from 'react';
import { useNavigate, useLocation, Navigate } from 'react-router-dom';
import { const_user } from './../utils/constants';

interface PrivateRouteProps {
    children: JSX.Element;
}

export const PrivateRoute = ({ children }: PrivateRouteProps) => {
    const location = useLocation();
    const isLogged = localStorage.getItem(const_user.user_email);
    console.log('private ', location, 'isLogged ', isLogged);

    return isLogged ? children : <Navigate to={'/login'} replace={true} />;
};
