import React from 'react';
import { NavBar } from '../../components/NavBar/NavBar';
import { SubBar } from '../../components/SubBar/SubBar';

export const WrapperRoute = ({ children }) => {
    return (
        <>
            <NavBar />
            <div style={{ maxWidth: '1440px', margin: '0 auto' }}>
                <div style={{ padding: '1rem 0' }}>{children}</div>
            </div>
        </>
    );
};
