import React from 'react';
import 'react-hot-loader';
import ReactDOM from 'react-dom';
import App from './App';
import './styles/global.scss';

ReactDOM.render(<App />, document.getElementById('root'));
