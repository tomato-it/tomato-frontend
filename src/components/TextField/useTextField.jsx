import React from 'react';
import InputAdornment from '@mui/material/InputAdornment';
import { cloneDeep } from './../../utils/utils';

export const useTextField = (props) => {
    let buttonState = cloneDeep(props);
    buttonState.className = '';
    delete buttonState.showLabelOptional;
    let labelOutsideState = {};
    let helperState = {};

    delete buttonState.labelInside;
    if (!props?.labelInside) {
        delete buttonState.label;
    }
    if (props?.labelInside && props?.showLabelOptional) {
        buttonState.label = `${buttonState.label} (Opcional)`;
    }
    buttonState.InputProps = {};
    // if (props?.readOnly) {
    //     buttonState.InputProps = { ...buttonState.InputProps, readOnly: true };
    //     buttonState.className = classes.readOnly;
    //     delete buttonState.readOnly;
    //     delete buttonState.error;
    // }
    if (props?.icon) {
        let style = {
            color: '#707070',
        };
        if (props?.size === 'medium') {
            style.fontSize = 34;
        }
        buttonState.InputProps = {
            ...buttonState.InputProps,
            endAdornment: (
                <InputAdornment position="end">
                    <props.icon style={style}></props.icon>
                </InputAdornment>
            ),
        };
        delete buttonState.icon;
    } else if (props?.prefix) {
        buttonState.InputProps = {
            ...buttonState.InputProps,
            startAdornment: <InputAdornment position="start">{props?.prefix}</InputAdornment>,
        };
    } else if (props?.suffix) {
        buttonState.InputProps = {
            ...buttonState.InputProps,
            inputProps: {
                style: {
                    textAlign: 'right',
                },
            },
            endAdornment: <InputAdornment position="end">{props?.suffix}</InputAdornment>,
        };
    }
    delete buttonState.helperText;

    if (props?.disabled && props?.error) {
        delete buttonState.error;
    }
    props?.readOnly && delete buttonState.disabled;
    if (props?.readOnly && props?.icon) {
        const { ...rest } = buttonState.InputProps;
        buttonState.InputProps = { ...rest };
    }

    const { labelInside, key, size, label, showLabelOptional } = props;
    labelOutsideState = { labelInside, key, size, label, showLabelOptional };

    helperState = {
        helperText: props.helperText,
        error: buttonState.error,
        key,
    };

    return { buttonState, labelOutsideState, helperState };
};
