import React from 'react';
import InputLabel from '@mui/material/InputLabel';

const LabelOutsideComponent = (props) => {
    if (!props?.labelInside) {
        return (
            <InputLabel
                shrink
                htmlFor={`TextField-custom-${props?.key}`}
                style={{
                    top: '-22px',
                    fontWeight: 'bold',
                    fontSize: props?.size === 'small' ? '1.2rem' : '1.3rem',
                    left: '-14px',
                }}
            >
                {props?.label}{' '}
                {props?.showLabelOptional && (
                    <span
                        style={{
                            fontWeight: 'normal',
                            fontSize: props?.size === 'small' ? '1.1rem' : '1.2rem',
                        }}
                    >
                        (Opcional)
                    </span>
                )}
            </InputLabel>
        );
    }
    return null;
};

export const LabelOutside = React.memo(LabelOutsideComponent);
