import React from 'react';
import FormHelperText from '@mui/material/FormHelperText';
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutlineOutlined';
import './HelperTextOutside.scss';

export const HelperTextOutside = (props) => {
    if (props?.helperText) {
        return (
            <FormHelperText
                id={`TextField-custom-${props?.key}`}
                error={props.error}
                className={`helperText ${props?.error && 'error'}`}
            >
                {props?.error && <ErrorOutlineIcon />}
                {props?.helperText}
            </FormHelperText>
        );
    }
    return null;
};
