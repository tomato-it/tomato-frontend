import React from 'react';
import PropTypes from 'prop-types';
import TextFieldStyled from '@mui/material/TextField';
import FormControl from '@mui/material/FormControl';
import { styled } from '@mui/material/styles';
import { useTextField } from './useTextField';
import { LabelOutside } from './LabelOutside';
import { HelperTextOutside } from './HelperTextOutside';

const TextFieldComponent = styled(TextFieldStyled)(({ theme }) => ({
    '& .MuiOutlinedInput-root': {
        borderRadius: 6,
        // backgroundColor: theme.palette.grey[100],
    },
    inputBase: {
        '& .MuiInputBase-root.Mui-disabled': {
            background: theme.palette.grey[200],
            color: theme.palette.grey[500],
        },
    },
    readOnly: {
        '& .MuiOutlinedInput-input': {
            paddingLeft: '0px',
            paddingRight: '0px',
        },
        '& .MuiOutlinedInput-root': {
            '& fieldset': {
                borderColor: 'transparent',
            },
            '&:hover fieldset': {
                borderColor: 'transparent',
            },
            '&.Mui-focused fieldset': {
                borderColor: 'transparent',
            },
        },
    },
}));

const TextField = ({ onChange, value, ...props }) => {
    // const classes = useStyles();
    const { buttonState, labelOutsideState, helperState } = useTextField(props);

    return (
        <FormControl
            style={{
                maxWidth: buttonState?.fullWidth ? 'unset' : 350,
                margin: '1.5rem 0 1rem 0',
                paddingBottom: '1rem',
            }}
        >
            <LabelOutside {...labelOutsideState} />
            <TextFieldComponent
                id={`TextField-custom-${props?.key}`}
                variant="outlined"
                {...buttonState}
                onChange={onChange}
                value={value}
            />
            <HelperTextOutside {...helperState} />
        </FormControl>
    );
};

TextField.defaultProps = {
    label: '',
    showLabelOptional: false,
    labelInside: false,
    readOnly: false,
    size: 'small',
    prefix: '',
    suffix: '',
    error: false,
};

TextField.propTypes = {
    size: PropTypes.oneOf(['small', 'medium']),
};

export default TextField;
