import React from 'react';
import './styles.scss';
import { useNavigate } from 'react-router-dom';
import { const_user } from './../../utils/constants';

export const NavBar = () => {
    const navigate = useNavigate();

    const isLogged = localStorage.getItem(const_user.user_email);

    const logout = () => {
        localStorage.removeItem(const_user.user_email);
        localStorage.removeItem(const_user.user_risk_profile);

        navigate('/login', { replace: true });
    };

    return (
        <nav className="NavBar-container">
            <div className="wrapper">
                {/* <p>$MART ADVI$OR</p> */}
                <p>SMART ADVISOR</p>

                {isLogged && <span onClick={logout}>Salir</span>}
            </div>
        </nav>
    );
};
