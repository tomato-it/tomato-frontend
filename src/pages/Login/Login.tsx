import React, { useEffect } from 'react';
import { Formik, useFormik } from 'formik';
import * as yup from 'yup';
import { Button } from '@mui/material';
import TextField from './../../components/TextField/TextField';
import EmailRoundedIcon from '@mui/icons-material/EmailRounded';
import { BFF_Login } from './../../BFF/Login';
import { useQuery } from 'react-query';
import { useNavigate } from 'react-router-dom';

import './Login.scss';
import axios from 'axios';
import { const_user } from './../../utils/constants';

const validationSchema = yup.object({
    email: yup.string().email('Ingrese un mail valido').required('Campo requerido'),
});

export const Login = () => {
    const navigate = useNavigate();
    const { handleSubmit, handleChange, values, touched, errors, isValid, handleBlur } = useFormik({
        initialValues: {
            email: '',
        },
        validationSchema: validationSchema,
        onSubmit: () => {
            refetch();
        },
    });

    const source = axios.CancelToken.source();
    const {
        data: resLogin,
        refetch,
        isRefetching,
        isRefetchError,
    } = useQuery(
        ['login', isValid],
        async () => {
            const body = { email: values.email };
            return await BFF_Login.login({ body, cancelToken: source.token });
        },
        {
            enabled: false,
            refetchOnWindowFocus: false,
            cacheTime: 0,
        }
    );

    useEffect(() => {
        return () => {
            source.cancel('login cancelled');
        };
    }, []);

    useEffect(() => {
        console.log(resLogin, !isRefetching, !isRefetchError);
        if (resLogin && !isRefetching && !isRefetchError) {
            localStorage.setItem(const_user.user_email, resLogin.email);
            navigate('/questions', { replace: true });
        }
    }, [resLogin, isRefetching, isRefetchError]);

    const buttonText = isRefetching ? 'Cargando...' : 'Ingresar';

    return (
        <div className="login-container">
            <form onSubmit={handleSubmit}>
                <h3>Bienvendo</h3>
                <TextField
                    id="email"
                    name="email"
                    size="medium"
                    label="Email"
                    value={values.email}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={touched.email && Boolean(errors.email)}
                    helperText={touched.email && errors.email}
                    autoComplete="off"
                    labelInside={false}
                    fullWidth
                    icon={EmailRoundedIcon}
                ></TextField>

                <Button type="submit" variant="contained" disabled={!isValid}>
                    {buttonText}
                </Button>

                {isRefetchError ? <p>{isRefetchError}</p> : ''}
            </form>
        </div>
    );
};

export default Login;
