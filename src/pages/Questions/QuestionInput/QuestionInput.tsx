import React from 'react';
import { Formik, Field, Form, useFormik } from 'formik';
// import TextField from './../../../components/TextField/TextField';
import TextField from '@mui/material/TextField';
import * as yup from 'yup';

interface Props {
    value: string | number;
    onChange: (value: string | number) => void;
    id: string;
}

export const QuestionInput = ({ value, onChange, id }: Props) => {
    const validationSchema = yup.object({
        [id]: yup.number().required('Campo requerido').min(18, 'Mínimo 18 años').max(95, 'Máximo 95 años'),
    });

    const onSubmit = (values: unknown) => {
        console.log(values);
    };
    const validateOnChange = (something: unknown): boolean | undefined => {
        console.log(something);
        return true;
    };

    const formik = useFormik({
        initialValues: {
            [id]: '',
        },
        validationSchema,
        onSubmit,
        validateOnChange: true,
    });

    return (
        <div>
            <form onSubmit={formik.handleSubmit}>
                <TextField
                    size="medium"
                    value={formik.values[id]}
                    onChange={formik.handleChange}
                    name={id}
                    type={'number'}
                    error={formik.touched[id] && Boolean(formik.errors[id])}
                    helperText={formik.touched[id] && formik.errors[id]}
                />
            </form>
        </div>
    );
};
