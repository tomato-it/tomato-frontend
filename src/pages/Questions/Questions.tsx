import React from 'react';
import Button from '@mui/material/Button';
import { Steps } from './Steps/Steps';
import { Question } from './Question/Question';
import './Questions.scss';
import { useQuestions } from './../../hooks/useQuestions';

export const Questions = () => {
    const {
        question,
        questionStatus,
        loadingQuestion,
        answerState,
        handleAnswerState,
        answerError,
        handleAnswerError,
        sendAnswer,
        loadingPostAnswer,
        errorPostAnswer,
    } = useQuestions();

    if (loadingQuestion) {
        return <h3>Loading...</h3>;
    }

    const onClickNext = () => {
        sendAnswer();
    };

    const buttonText = loadingPostAnswer ? 'Cargando...' : 'Siguiente';

    return (
        <div className="Questions">
            <div className="Questions__header">
                <h3>Contanos tu experiencia con las inversiones</h3>
                <Steps currentStep={questionStatus.step || 1} total={questionStatus?.total || 1} />
            </div>
            {question && (
                <Question
                    format={question.format}
                    label={question?.label}
                    options={question.options}
                    state={answerState}
                    onChangeState={handleAnswerState}
                    type={question.type}
                    error={answerError}
                    onChangeError={handleAnswerError}
                />
            )}
            <div className="Questions__footer">
                <Button
                    onClick={onClickNext}
                    variant="contained"
                    disabled={Boolean(answerError) || !answerState || loadingPostAnswer}
                >
                    {buttonText}
                </Button>
            </div>
        </div>
    );
};

export default Questions;
