import * as React from 'react';
import CircularProgress, { CircularProgressProps } from '@mui/material/CircularProgress';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import './Steps.scss';

interface CircularProgressWithLabelProps extends CircularProgressProps {
    currentStep: number | string;
    total: number;
}

function CircularProgressWithLabel(props: CircularProgressWithLabelProps) {
    const { currentStep, total, ...rest } = props;

    return (
        <Box sx={{ position: 'relative', display: 'inline-flex' }}>
            <CircularProgress
                variant="determinate"
                {...rest}
                value={(+currentStep * 100) / (total + 1)}
                color="primary"
            />
            <Box
                sx={{
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    position: 'absolute',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}
            >
                <Typography
                    variant="caption"
                    component="div"
                    color="text.secondary"
                >{`${currentStep}/${total}`}</Typography>
            </Box>
        </Box>
    );
}

interface StepsProps {
    total: number;
    currentStep: number | string;
}

export const Steps = (props: StepsProps) => {
    return (
        <div className="Steps">
            <p>Pasos</p>
            <CircularProgressWithLabel {...props} />
        </div>
    );
};
