import React, { ChangeEvent, useEffect, useState } from 'react';
import TextField from './../../../components/TextField/TextField';
import { optionText, optionRange, QuestionFormat, QuestionType } from './../../../interfaces/question';
import * as yup from 'yup';
import './Question.scss';
import { isOnlyNumber } from './../../../utils/number';

const validateInputNum = yup
    .number()
    .required('Campo requerido')
    .min(18, 'Debe ser mayor de 18 años')
    .max(95, 'Porfavor, ingrese una edad valida');
interface QuestionProps {
    label: string;
    options?: (optionText | optionRange)[];
    format: QuestionFormat;
    state: string;
    onChangeState: (value: string) => void;
    type: QuestionType;
    error: string;
    onChangeError: (value: string) => void;
}

export const Question = ({
    label = '',
    options = [],
    format,
    type,
    state,
    onChangeState,
    error,
    onChangeError,
}: QuestionProps) => {
    const onChangeInput = (e: ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        if (isOnlyNumber({ value }) || value === '') {
            onChangeState(value);
            if (value !== '') {
                validateInputNum
                    .validate(value)
                    .then((res) => {
                        onChangeError('');
                    })
                    .catch((err) => {
                        console.log('err ', err);
                        onChangeError(err.message);
                    });
            } else {
                onChangeError('Campo requerido');
            }
        }
    };

    return (
        <div className="Question__container">
            <p className="Question__label">{label}</p>

            <div className="Question__options">
                {type === 'input' && format === 'number' ? (
                    <TextField
                        size="medium"
                        type="number"
                        value={state}
                        onChange={onChangeInput}
                        error={Boolean(error)}
                        helperText={Boolean(error) && error}
                        autoComplete="off"
                    />
                ) : type === 'multiple' ? (
                    options.map((op, index) => {
                        if (format === 'text') {
                            const option = op as optionText;
                            return (
                                <div
                                    key={`${op.weight}-${index}`}
                                    className={`option ${state === option.text && 'selected'}`}
                                    onClick={() => onChangeState(op.text)}
                                >
                                    {option.text}
                                </div>
                            );
                        }
                        if (format === 'range') {
                            const option = op as optionRange;
                            return (
                                <div
                                    key={`${op.weight}-${index}`}
                                    className={`option ${state === option.text && 'selected'}`}
                                    onClick={() => onChangeState(option.text)}
                                >
                                    {option.text}
                                </div>
                            );
                        }
                        return null;
                    })
                ) : null}
            </div>
        </div>
    );
};
