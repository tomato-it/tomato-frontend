import { FormControl, InputAdornment, OutlinedInput, Slider, Tooltip, Typography } from '@mui/material';
import { Line } from 'react-chartjs-2';
import React from 'react';
import './ChartPortfolio.styles.scss';
import { ChartPortfolioFilters } from '../Simulator.constant';
import { maskFuntions } from './../../../utils/utils';
import { styled } from '@mui/material/styles';
import {
    ChartData,
    CoreChartOptions,
    DatasetChartOptions,
    ElementChartOptions,
    PluginChartOptions,
    LineControllerChartOptions,
    ScaleChartOptions,
} from 'chart.js';
import { lineChartData } from './../../../interfaces/charts';
import { _DeepPartialObject } from 'chart.js/types/utils';

const labels: string[] = ['2 Jan', '3 Jan', '4 Jan', '5 Jan', '6 Jan'];
const data = [2334, 422, 4423, 4443, 3223];

const dataSet = {
    labels,
    datasets: [
        {
            label: 'Concervative',
            data,
            borderColor: '#e7ff15',
        },
    ],
};

const marks = [
    {
        value: 1000,
        label: '$1.000',
    },
    {
        value: 500000,
        label: '$500.000',
    },
    {
        value: 1000000,
        label: '$1.000.000',
    },
];

const valuetext = (value: number) => {
    return `$ ${value}`;
};

const valueLabelFormat = (value: number) => {
    return <p style={{ fontSize: '1.2rem' }}>{`$ ${maskFuntions.applyMask(value)}`}</p>;
};

const CapitalSlider = styled(Slider)(({ theme }) => ({
    color: '#ebff3b',
    height: 2,
    padding: '1rem 0',
    marginTop: '1.5rem',
    '& .MuiSlider-thumb': {
        backgroundColor: '#e7ff15',
        height: 18,
        width: 18,
    },
    '& .MuiSlider-valueLabel': {
        fontSize: 12,
        fontWeight: 'normal',
        top: -6,
        backgroundColor: 'unset',
        color: theme.palette.text.primary,
        '&:before': {
            display: 'none',
        },
        '& *': {
            background: 'transparent',
            color: theme.palette.mode === 'dark' ? '#fff' : '#000',
        },
    },
    '& .MuiSlider-markLabel': {
        marginTop: '1rem',
    },
}));

type lineChartOption =
    | _DeepPartialObject<
          CoreChartOptions<'line'> &
              ElementChartOptions<'line'> &
              PluginChartOptions<'line'> &
              DatasetChartOptions<'line'> &
              ScaleChartOptions &
              LineControllerChartOptions
      >
    | undefined;

const optionsLineChart: lineChartOption = {
    plugins: {
        legend: {
            display: false,
        },
    },
};

interface ChartPortfolioProps {
    balance: { totalBalance: number; variation: number; amountVariation: number };
    lineChartData: lineChartData;
    filterConfig: {
        value: string;
        onChange: (time: string) => void;
    };
    initialCapitalConfig: {
        value: number;
        onChange: (capital: number) => void;
    };
}

export const ChartPortfolio = ({
    filterConfig: { value, onChange },
    initialCapitalConfig,
    lineChartData,
    balance,
}: ChartPortfolioProps) => {
    const { amountVariation, variation, totalBalance } = balance;

    const smartPorfolioText = `${amountVariation >= 0 ? '+' : '-'}$${maskFuntions.applyMask(
        Math.abs(amountVariation)
    )} / ${variation > 0 ? '+' : ''}${variation}%`;
    return (
        <div className="ChartPortfolio-container">
            <div className="balance">
                <div className="col">
                    <p className="amount">${maskFuntions.applyMask(initialCapitalConfig.value)}</p>
                    <p className="label">Your investment</p>
                </div>
                <div className="col">
                    <p className="amount">{smartPorfolioText}</p>
                    <p className="label">Smart portfolio</p>
                </div>
                <div className="col">
                    <p className="amount large">${maskFuntions.applyMask(totalBalance)}</p>
                    <p className="label">Total balance</p>
                </div>
            </div>
            <div className="chart">
                {lineChartData && <Line data={lineChartData} options={optionsLineChart}></Line>}
            </div>

            <div className="little-form">
                <div className="row">
                    <div className="col slider-container">
                        <Typography className="little-form-label" gutterBottom>
                            Initial Capital
                        </Typography>
                        <CapitalSlider
                            className="slider-capital"
                            name="initialCapital"
                            aria-label="initial-capital"
                            min={1000}
                            max={1000000}
                            value={initialCapitalConfig.value}
                            step={1000}
                            valueLabelDisplay="on"
                            marks={marks}
                            getAriaValueText={valuetext}
                            valueLabelFormat={valueLabelFormat}
                            onChange={(e: any) => {
                                const value = e.target.value;
                                initialCapitalConfig.onChange(Number(value));
                            }}
                        />
                    </div>
                    <div className="col select-timeline">
                        {ChartPortfolioFilters.map((time) => (
                            <span
                                key={time}
                                className={`${time === value ? 'selected' : ''}`}
                                onClick={() => onChange(time)}
                            >
                                {time}
                            </span>
                        ))}
                    </div>
                </div>
            </div>
        </div>
    );
};
