export type IChartPortfolioFilters = '2008' | '5y' | '3y' | '1y' | '1q' | '1m';

export const ChartPortfolioFilters: IChartPortfolioFilters[] = ['2008', '5y', '3y', '1y', '1q', '1m'];
