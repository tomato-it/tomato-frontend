import { Table, TableBody, TableCell, tableCellClasses, TableHead, TableRow } from '@mui/material';
import React from 'react';
import './InvestmentPortfolio.scss';
import { styled } from '@mui/material/styles';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        color: theme.palette.common.white,
        border: 0,
        backgroundColor: theme.palette.common.black,
        '&:nth-of-type(1)': {
            borderRadius: '12px 0 0 12px',
        },
        '&:last-child': {
            borderRadius: '0 12px 12px 0',
        },
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
        border: 0,
    },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    background: 'transparent',
    border: 0,
}));

interface TableCustomProps {
    data: any[];
    colums: {
        key: string;
        label: string;
        transform?: (obj: any) => string | number;
    }[];
}

const TableCustom = ({ data = [], colums }: TableCustomProps) => {
    return (
        <Table style={{ width: '75%', margin: 'auto' }} aria-label="customized table">
            <TableHead>
                <TableRow>
                    {colums.map(({ key, label }) => (
                        <StyledTableCell key={key} align="center">
                            {label}
                        </StyledTableCell>
                    ))}
                </TableRow>
            </TableHead>
            <TableBody>
                {data.map((dataItem) => {
                    return (
                        <StyledTableRow key={dataItem?.id || dataItem?.[colums[0].key]}>
                            {colums.map((col) => (
                                <StyledTableCell key={dataItem[col.key]} component="th" scope="row" align="center">
                                    {col.transform ? col.transform(dataItem[col.key]) : dataItem[col.key]}
                                </StyledTableCell>
                            ))}
                        </StyledTableRow>
                    );
                })}
            </TableBody>
        </Table>
    );
};

export const InvestmentPortfolio = ({ holdings = [] }) => {
    const colums = [
        {
            key: 'ticker',
            label: 'Name',
        },
        {
            key: 'percentage',
            label: 'Target',
            transform: (element: string | number) => {
                return `${Number(+element * 100)} %`;
            },
        },
    ];

    return (
        <div className="InvestmentPortfolio-container">
            <TableCustom data={holdings} colums={colums} />
        </div>
    );
};
