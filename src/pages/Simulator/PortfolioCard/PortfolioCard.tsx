import React from 'react';
import './PortfolioCard.scss';

interface PortfolioCardProps {
    title: string;
    selected: boolean;
    onSelectPortfolio: (slug: string) => void;
}

export const PortfolioCard = ({ title, selected, onSelectPortfolio }: PortfolioCardProps) => {
    return (
        <div className={`card-item ${selected ? 'selected' : ''}`} onClick={() => onSelectPortfolio(title)}>
            <p className="card-item-title">{title}</p>
        </div>
    );
};
