import React from 'react';
import { useSimulator } from '../../hooks/useSimulator';
import { ChartPortfolio } from './ChartPortfolio/ChartPortfolio';
import { InvestmentPortfolio } from './InvestmentPortfolio/InvestmentPortfolio';
import { PieChartPortfolio } from './PieChartPortfolio/PieChartPortfolio';
import { SelectPortfolio } from './SelectPortfolio/SelectPortfolio';
import './Simulator.scss';
import wrongSVG from './../../../public/images/wrong.svg';

export const Simulator = () => {
    const {
        loading,
        errorMessage,
        portfolios,
        selectedPortfolio,
        onSelectPortfolio,
        lineChart: { filter, initialCapital, data, balance },
    } = useSimulator();

    const holdings = portfolios.find((p) => p.slug === selectedPortfolio)?.holdings || [];

    if (loading) {
        return (
            <div className="simulator-container">
                <h2>Loading...</h2>
            </div>
        );
    }

    if (errorMessage) {
        return (
            <div className="wrong-container">
                <h2>Oops... something is wrong</h2>
                <img src={wrongSVG} alt="something is wrong" />
            </div>
        );
    }

    return (
        <div className="simulator-container">
            <SelectPortfolio
                portfolios={portfolios}
                onSelectPortfolio={onSelectPortfolio}
                selected={selectedPortfolio}
            />
            <div className="section">
                <div className="section-title">
                    <p>{selectedPortfolio}</p>
                </div>
                <ChartPortfolio
                    balance={balance}
                    lineChartData={data}
                    filterConfig={filter}
                    initialCapitalConfig={initialCapital}
                />
                <PieChartPortfolio holdings={holdings} />
                <InvestmentPortfolio holdings={holdings} />
            </div>
        </div>
    );
};

export default Simulator;
