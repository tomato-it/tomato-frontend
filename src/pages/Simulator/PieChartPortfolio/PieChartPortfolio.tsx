import React from 'react';
import './PieChartPortfolio.scss';
import { Doughnut } from 'react-chartjs-2';
import { Holding } from './../../../interfaces/holding';
import { Capitalize } from './../../../utils/string/capitalize';
import { generateRandomColor } from './../../../utils/utils';
import { _DeepPartialObject } from 'chart.js/types/utils';
import {
    CoreChartOptions,
    DatasetChartOptions,
    ElementChartOptions,
    PluginChartOptions,
    ScaleChartOptions,
    DoughnutControllerChartOptions,
    ChartData,
} from 'chart.js';

const transformData = (holdingsData: Holding[]) => {
    const labels = holdingsData.map((h) => Capitalize(h.ticker));
    const data = holdingsData.map((h) => h.percentage);
    const backgroundColors = holdingsData.map(() => generateRandomColor());

    const dataDoughnut: ChartData<'doughnut'> = {
        labels,
        datasets: [
            {
                label: '% Quote',
                data,
                backgroundColor: backgroundColors,
                borderColor: backgroundColors,
                borderWidth: 0,
            },
        ],
    };
    return dataDoughnut;
};

type optionsPieChart =
    | _DeepPartialObject<
          CoreChartOptions<'doughnut'> &
              ElementChartOptions<'doughnut'> &
              PluginChartOptions<'doughnut'> &
              DatasetChartOptions<'doughnut'> &
              ScaleChartOptions &
              DoughnutControllerChartOptions
      >
    | undefined;

const optionsChart: optionsPieChart = {
    cutout: 100,
    plugins: {
        legend: {
            display: false,
        },
    },
};

interface PieChartPortfolioProps {
    holdings: Holding[];
}

export const PieChartPortfolioComponent = ({ holdings = [] }: PieChartPortfolioProps) => {
    const dataDoughnut = transformData(holdings);

    return (
        <div className="PieChartPortfolio-container">
            <div className="pie">
                <Doughnut data={dataDoughnut} options={optionsChart} />
            </div>
        </div>
    );
};

export const PieChartPortfolio = React.memo(PieChartPortfolioComponent);
