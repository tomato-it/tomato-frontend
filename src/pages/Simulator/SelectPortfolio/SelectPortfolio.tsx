import React from 'react';
import { PortfolioCard } from '../PortfolioCard/PortfolioCard';
import './SelectPortfolio.styles.scss';
import { Portfolio } from 'src/interfaces/portfolio';

interface SelectPortfolioProps {
    portfolios: Portfolio[];
    selected: string;
    onSelectPortfolio: (slug: string) => void;
}

export const SelectPortfolio = ({ portfolios, selected, onSelectPortfolio }: SelectPortfolioProps) => {
    return (
        <div className="SelectPortfolio-container">
            <p className="title">Select your portfolio</p>
            <div className="card-wrapper">
                {portfolios.map(({ slug }) => (
                    <PortfolioCard
                        key={slug}
                        title={slug}
                        selected={selected === slug}
                        onSelectPortfolio={onSelectPortfolio}
                    />
                ))}
            </div>
        </div>
    );
};
