import axios from 'axios';
import { useState, useEffect } from 'react';
import { BFF_Questions } from './../BFF/Questions';
import { IQuestion } from './../interfaces/question';
import { useQuery } from 'react-query';
import { useSearchParams, useNavigate } from 'react-router-dom';
import { parse, stringify } from 'query-string';
import { const_user } from './../utils/constants';

const stepOne = { step: '1' };

interface QuestionStatus {
    next: string;
    prev: string;
    total: number;
}

export const useQuestions = () => {
    const navigate = useNavigate();
    const [searchParam, setSearchParam] = useSearchParams();
    const step = searchParam.get('step');

    const [question, setQuestion] = useState<IQuestion>();
    const [questionStatus, setQuestionStatus] = useState<QuestionStatus>();
    let source = axios.CancelToken.source();
    const { isLoading, isError } = useQuery(
        ['getQuestion', step],
        async () => {
            const res = await BFF_Questions.getQuestion({ cancelToken: source.token, step: Number(step) });
            const { current, ...rest } = res;
            setQuestion(current);
            setQuestionStatus({ ...rest });
        },
        {
            enabled: step !== null,
            refetchOnWindowFocus: false,
        }
    );
    const [answerState, setAnswerState] = useState('');
    const [answerError, setAnswerError] = useState('');

    const postAnswer = async () => {
        if (!question?.id || !answerState) {
            throw new Error('questionId and answerState is required');
            return;
        }
        const email = getUserEmail();
        const body = {
            email,
            answer: {
                questionId: question?.id,
                value: answerState,
            },
        };
        return await BFF_Questions.postAnswer({ body, cancelToken: source.token });
    };
    const {
        refetch: sendAnswer,
        isRefetching: loadingPostAnswer,
        isRefetchError: errorPostAnswer,
        data: resPostAnswer,
    } = useQuery(['postAnswer', question?.id], postAnswer, {
        enabled: false,
        refetchOnWindowFocus: false,
        cacheTime: 0,
    });

    useEffect(() => {
        if (!step) setSearchParam(stepOne);

        return () => {
            source.cancel('getQuestion Cancelled');
        };
    }, []);

    useEffect(() => {
        if (resPostAnswer && question?.id) {
            if (resPostAnswer.questionId === question?.id) {
                onNextQuestion();
            }
        }
    }, [resPostAnswer, question?.id]);

    const getUserEmail = () => {
        return localStorage.getItem('user_email') || '';
    };

    const onNextQuestion = () => {
        if (questionStatus?.next && step && answerState) {
            setSearchParam({ step: String(+step + 1) });
            setAnswerState('');
        } else if (!questionStatus?.next && step && questionStatus?.total && Number(step) === questionStatus?.total) {
            source = axios.CancelToken.source();
            BFF_Questions.getRiskProfile({ email: getUserEmail(), cancelToken: source.token })
                .then((res) => {
                    localStorage.setItem(const_user.user_risk_profile, res.data.defaultPortfolio);
                    navigate('/simulator', { replace: true });
                })
                .catch((err) => {
                    console.log(err);
                });
        }
    };

    const handleAnswerState = (value: string) => {
        setAnswerState(value);
    };

    return {
        loadingQuestion: isLoading,
        errorLoadingQuestion: isError,
        question,
        questionStatus: {
            ...questionStatus,
            step,
        },
        // onNextQuestion,
        answerState,
        handleAnswerState,
        answerError,
        handleAnswerError: (err: string) => setAnswerError(err),
        sendAnswer,
        loadingPostAnswer,
        errorPostAnswer,
    };
};
