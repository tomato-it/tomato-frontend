import { useState, useEffect, useMemo, useRef } from 'react';
import { ChartPortfolioFilters, IChartPortfolioFilters } from '../pages/Simulator/Simulator.constant';
import { BFF_Silumator } from '../BFF/Simulator';
import { Portfolio } from '../interfaces/portfolio';
import { lineChartData } from '../interfaces/charts';
import debounce from 'lodash.debounce';
import { roundNumber } from '../utils/number/roundNumber';
import axios, { CancelToken } from 'axios';
import { const_user } from './../utils/constants';

export const useSimulator = () => {
    const [selectedPortfolio, setSelectedPortfolio] = useState('');
    const [portfolios, setPortfolios] = useState<Portfolio[]>([]);
    const [errorMessage, setErrorMessage] = useState('');
    const [selectedTime, setSelectedTime] = useState<IChartPortfolioFilters>(
        ChartPortfolioFilters[ChartPortfolioFilters.length - 1]
    );
    const [initialCapital, setInitialCapital] = useState<number>(10000);
    const [lineChartData, setLineChartData] = useState<lineChartData>();
    const [balance, setBalance] = useState({ totalBalance: 0, variation: 0, amountVariation: 0 });
    const [loading, setLoading] = useState(true);

    const isMounted = useRef<null | true | false>(null);

    const loadPorfolios = async (cancelToken: CancelToken) => {
        setLoading(true);
        try {
            const res: Portfolio[] = await BFF_Silumator.getPorfolios({ cancelToken });
            if (isMounted.current) {
                const defaultProfile = localStorage.getItem(const_user.user_risk_profile) || res[0].slug;
                setSelectedPortfolio(defaultProfile);
                setPortfolios(res);
                setLoading(false);
            }
        } catch (error) {
            setErrorMessage('Error to load portfolios');
            setLoading(false);
        }
    };

    useEffect(() => {
        if (isMounted.current && selectedPortfolio) {
            debounceGetHistory(initialCapital, selectedTime, selectedPortfolio);
        }
    }, [selectedPortfolio]);

    const onSelectPortfolio = (slug: string) => {
        setSelectedPortfolio(slug);
    };

    const getHistoryTime = (time: IChartPortfolioFilters) => {
        let timeToSet = 0;

        const today = new Date();
        const dayMiliseconds = 86400000;

        switch (time) {
            case '1m':
                timeToSet = today.getTime() - dayMiliseconds * 60;
                break;
            case '1q':
                timeToSet = today.getTime() - dayMiliseconds * 90;
                break;
            case '1y':
                timeToSet = today.getTime() - dayMiliseconds * 365;
                break;
            case '3y':
                timeToSet = today.getTime() - dayMiliseconds * (365 * 3);
                break;
            case '5y':
                timeToSet = today.getTime() - dayMiliseconds * (365 * 5);
                break;
            case '2008':
                timeToSet = new Date('2008-01-01').getTime();
                break;
            default:
                timeToSet = today.getTime() - dayMiliseconds * 31;
                break;
        }

        const timeISOString = new Date(timeToSet).toISOString().split('T')[0];

        return timeISOString;
    };

    const onSelectTime = (time: IChartPortfolioFilters) => {
        setSelectedTime(time);
        debounceGetHistory(initialCapital, time, selectedPortfolio);
    };

    const onChangeInitialCapital = (capital: number) => {
        setInitialCapital(capital);
        debounceGetHistory(capital, selectedTime, selectedPortfolio);
    };

    const getHistory = (amount: number, time: IChartPortfolioFilters, portfolioSlug: string) => {
        const fromDate = getHistoryTime(time);
        BFF_Silumator.getHistoryPortfolio({ amount, time: fromDate, portfolioSlug })
            .then((res) => {
                setLineChartData(res);
                const resData = res.datasets[0].data;
                const initialBalance = resData[0];
                const totalBalance = resData[resData.length - 1];
                const variation = roundNumber((totalBalance * 100) / initialBalance - 100);
                const amountVariation = roundNumber(totalBalance - initialBalance);
                setBalance({ totalBalance, variation, amountVariation });
                return;
            })
            .catch((error) => null);
    };

    const debounceGetHistory = useMemo(() => debounce(getHistory, 400), []);

    useEffect(() => {
        isMounted.current = true;
        const source = axios.CancelToken.source();
        loadPorfolios(source.token);
        return () => {
            isMounted.current = false;
            source.cancel('Promise cancelled');
            debounceGetHistory.cancel();
        };
    }, []);

    return {
        loading,
        portfolios,
        errorMessage,
        selectedPortfolio,
        onSelectPortfolio,
        lineChart: {
            balance,
            data: lineChartData,
            filter: {
                value: selectedTime,
                onChange: onSelectTime,
            },
            initialCapital: {
                value: initialCapital,
                onChange: onChangeInitialCapital,
            },
        },
    };
};
