import { Holding } from './holding';

export interface Portfolio {
    slug: string;
    holdings: Holding[];
}
