export interface Holding {
    percentage: number;
    ticker: string;
}
