import { ChartData } from 'chart.js';

export type lineChartData = ChartData<'line', number[], string>;
