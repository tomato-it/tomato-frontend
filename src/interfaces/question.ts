export type QuestionFormat = 'number' | 'text' | 'range';
export type QuestionType = 'input' | 'multiple';
export interface optionRange {
    min: number;
    max: number;
    weight: number;
    text: string;
}

export interface optionText {
    text: string;
    weight: number;
}

export interface IQuestion {
    id: number;
    label: string;
    options?: (optionText | optionRange)[];
    format: QuestionFormat;
    type: QuestionType;
}
