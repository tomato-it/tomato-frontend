import { REACT_APP_API } from '../../config/config.env';
import axios, { CancelToken } from 'axios';

interface loginProps {
    body: {
        email: string;
    };
    cancelToken: CancelToken;
}

export const login = async ({ body, cancelToken }: loginProps) => {
    try {
        const { data } = await axios.post(`${REACT_APP_API}/users`, body, { cancelToken });
        return data;
    } catch (error) {
        console.log('Error login ', error);
        return error;
    }
};
