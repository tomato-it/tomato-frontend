import { getPorfolios } from './getPortfolios';
import { getHistoryPortfolio } from './getHistoryPortfolio';

export const BFF_Silumator = {
    getPorfolios,
    getHistoryPortfolio,
};
