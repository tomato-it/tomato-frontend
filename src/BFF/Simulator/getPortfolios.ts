import { REACT_APP_API } from '../../config/config.env';
import axios, { CancelToken } from 'axios';

interface getPorfoliosProps {
    cancelToken: CancelToken;
}

export const getPorfolios = async ({ cancelToken }: getPorfoliosProps) => {
    try {
        const { data } = await axios.get(`${REACT_APP_API}/portfolios/allocations`, { cancelToken });
        return data;
    } catch (error) {
        console.log('Error getPorfolios', error);
        return [];
    }
};
