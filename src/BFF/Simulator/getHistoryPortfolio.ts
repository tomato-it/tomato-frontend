import { REACT_APP_API } from '../../config/config.env';
import axios from 'axios';
import { roundNumber } from '../../utils/number/roundNumber';
import { lineChartData } from './../../interfaces/charts';

interface getHistoryPortfolioArgs {
    time: string;
    amount: number;
    portfolioSlug: string;
}

export async function getHistoryPortfolio({
    time,
    amount,
    portfolioSlug,
}: getHistoryPortfolioArgs): Promise<lineChartData> {
    try {
        const { data: dataResponse } = await axios.get(
            `${REACT_APP_API}/portfolios/${portfolioSlug}/history?from=${time}`
        );
        const { history = [], variation = 0 } = dataResponse;

        const labels: any[] = [];
        const data: any[] = [];
        let currentInvestment = amount;
        for (let currentDay = 0; currentDay < history.length; currentDay++) {
            const day = history[currentDay].date;
            const currentVariation = 1 + history[currentDay].variation;
            currentInvestment = roundNumber(currentInvestment * currentVariation);

            labels.push(day);
            data.push(currentInvestment);
        }

        const response: lineChartData = {
            labels,
            datasets: [
                {
                    label: portfolioSlug,
                    data,
                    borderColor: '#e7ff15',
                    tension: 0.3,
                    pointBorderWidth: 0,
                },
            ],
        };

        return response;
    } catch (error) {
        console.log('Error getPorfolios', error);
        return {
            labels: ['1', '2'],
            datasets: [
                {
                    label: portfolioSlug,
                    data: [1, 2],
                    borderColor: '#e7ff15',
                },
            ],
        };
    }
}
