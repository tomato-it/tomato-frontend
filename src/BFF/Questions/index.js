import { getQuestion } from './getQuestion';
import { postAnswer } from './postAnswer';
import { getRiskProfile } from './getRiskProfile';

export const BFF_Questions = {
    getQuestion,
    postAnswer,
    getRiskProfile,
};
