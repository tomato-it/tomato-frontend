import { REACT_APP_API } from '../../config/config.env';
import axios, { CancelToken } from 'axios';

interface getRiskProfileProps {
    email: string;
    cancelToken: CancelToken;
}

export const getRiskProfile = async ({ email, cancelToken }: getRiskProfileProps) => {
    try {
        const { data } = await axios.post(`${REACT_APP_API}/onboarding/user/risk/profile`, { email }, { cancelToken });
        return data;
    } catch (error) {
        console.log('Error getRiskProfile ', error);
        return error;
    }
};
