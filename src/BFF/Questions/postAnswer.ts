import { REACT_APP_API } from '../../config/config.env';
import axios, { CancelToken } from 'axios';

interface getQuestionProps {
    body: {
        email: string;
        answer: {
            questionId: number;
            value: unknown;
        };
    };
    cancelToken: CancelToken;
}

export const postAnswer = async ({ body, cancelToken }: getQuestionProps) => {
    try {
        const { data } = await axios.post(`${REACT_APP_API}/onboarding/user/answers`, body, { cancelToken });
        return data;
    } catch (error) {
        console.log('Error postAnswer', error);
        return error;
    }
};
