import { REACT_APP_API } from '../../config/config.env';
import axios, { CancelToken } from 'axios';

interface getQuestionProps {
    step: number;
    cancelToken: CancelToken;
}

export const getQuestion = async ({ step, cancelToken }: getQuestionProps) => {
    try {
        const { data } = await axios.get(`${REACT_APP_API}/onboarding/questions/${step}`, { cancelToken });
        return data;
    } catch (error) {
        console.log('Error getQuestion', error);
        return {};
    }
};
