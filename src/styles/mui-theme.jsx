import { createTheme } from '@mui/material/styles';
import InterBold from './../../public/fonts/Inter/Inter-Bold.ttf';
import InterMedium from './../../public/fonts/Inter/Inter-Medium.ttf';
import InterLight from './../../public/fonts/Inter/Inter-Light.ttf';

export const MuiTheme = createTheme({
    palette: {
        mode: 'dark',
        primary: {
            main: '#e7ff15',
        },
        // secondary: {
        //     main: '#0d1e3f',
        // },
        background: {
            default: '#161613',
            paper: '#161613',
        },
    },
    typography: {
        fontFamily: 'Inter',
    },
    components: {
        MuiCssBaseline: {
            styleOverrides: `
          @font-face {
            font-family: 'Inter';
            font-style: bold;
            font-display: swap;
            font-weight: 700;
            src: local('Inter'), url(${InterBold}) format('truetype');
          };
          @font-face {
            font-family: 'Inter';
            font-style: normal;
            font-display: swap;
            font-weight: 500;
            src: local('Inter'), url(${InterMedium}) format('truetype');
          };
          @font-face {
            font-family: 'Inter';
            font-style: lighter;
            font-display: swap;
            font-weight: 300;
            src: local('Inter'), url(${InterLight}) format('truetype');
          };
        `,
        },
    },
});
