import { Navigation } from './routes/Navigation';
import { MuiTheme } from './styles/mui-theme';
import { ThemeProvider } from '@mui/material/styles';
import { CssBaseline } from '@mui/material';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
    ArcElement,
} from 'chart.js';
import './App.scss';
import backgroundGradient from './../public/images/gradient-right-dark.svg';
import { QueryClientProvider, QueryClient } from 'react-query';

ChartJS.register(CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Legend, ArcElement);

const queryClient = new QueryClient();

const App = () => {
    return (
        <div className="App">
            {/* <img className="gradient-r" src={backgroundGradient} alt="graddient" /> */}
            <ThemeProvider theme={MuiTheme}>
                <CssBaseline />
                <QueryClientProvider client={queryClient}>
                    <Navigation />
                </QueryClientProvider>
            </ThemeProvider>
        </div>
    );
};

export default App;
