declare module '*.jpg';
declare module '*.svg' {
    const content: string;
    export default content;
}
declare module 'lodash.debounce';
declare module '@ungap/structured-clone';
declare module 'lodash.clonedeep';
