import structuredClone from '@ungap/structured-clone';
import cloneDeepIm from 'lodash.clonedeep';

export const cloneDeep = (value: unknown) => cloneDeepIm(value);

export const generateRandomColor = () => {
    const letters = '0123456789ABCDEF'.split('');
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.round(Math.random() * 15)];
    }
    return color;
};

const _isEmptyValue = (value: string | number) => !value && value !== 0;

export const maskFuntions = {
    removeMask: (value: string) => {
        if (_isEmptyValue(value)) return '';
        return Number(String(value).split('.').join(''));
    },
    applyMask: (value: number) => {
        if (_isEmptyValue(value)) return '';
        const intMask = new Intl.NumberFormat('es-Ar');
        return intMask.format(value);
    },
};

export const getRandomArbitrary = (min: number, max: number) => {
    return Math.random() * (max - min) + min;
};
