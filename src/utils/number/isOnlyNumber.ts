interface Props {
    value: number | string;
    commas?: boolean;
    dots?: boolean;
}

export const isOnlyNumber = ({ value, commas = false, dots = false }: Props) => {
    if (!value) return;

    let basicRegex = RegExp(/^[0-9]+$/);
    if (commas) {
        basicRegex = RegExp(/^[,0-9]+$/);
    } else if (dots) {
        basicRegex = RegExp(/^[.0-9]+$/);
    }

    const rgNum = new RegExp(basicRegex);
    return rgNum.test(String(value));
};
