import type { Config } from '@jest/types';

// Sync object
const config: Config.InitialOptions = {
    verbose: true,
    moduleNameMapper: {
        '\\.(css|scss)$': '<rootDir>/src/tests/__mocks__/styleMock.js',
    },
    setupFilesAfterEnv: ['<rootDir>/src/tests/setupTest.js'],
    setupFiles: ['dotenv/config'],
};
export default config;
